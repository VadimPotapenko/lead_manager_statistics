<?php
require_once __DIR__.'/db_conf.php';
require_once __DIR__.'/libs/crest/CRestPlus.php';
require_once __DIR__.'/libs/debugger/Debugger.php';
define ('HANDLER', 'https://demo.nicedo.ru/local/php_interface/rodion/handlers/event_handler.php'); // обработчик события 'новое дело' (меняем портал demo.nicedo.ru, путь тот же)
$mysqli = new mysqli(DB_CONF['host'], DB_CONF['user'], DB_CONF['pass'], DB_CONF['db']);
if ($mysqli->connect_errno) die($mysqli->connect_error);
$tables = array('lead_table', 'block_table');
foreach ($tables as $table) {
	if (!$result = $mysqli->query("SHOW TABLES FROM sitemanager LIKE '".$table."';")) die($mysqli->error);
	while( $row = mysqli_fetch_assoc($result))
        $isTables[] = $row;
}
$result->free();

if (!isset($isTables) || empty($isTables)) {
	$sqls = array(
		'CREATE TABLE `lead_table` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`lead_id` int(11) NOT NULL,
			`is_worked` int(1),
			`responsible` int(11) NOT NULL,
			`date_create` datetime NOT NULL,
			`date_update` datetime NOT NULL,
			PRIMARY KEY (`id`)
		);',
		'CREATE TABLE `block_table` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`responsible` int(11) NOT NULL,
			`date_block` datetime NOT NULL,
			`date_unblock` datetime NOT NULL,
			PRIMARY KEY (`id`)
		);'
	);

	foreach ($sqls as $command)
		if (!$result = $mysqli->query($command)) die($mysqli->error);
}

if(isset($_REQUEST['PLACEMENT']) && !file_exists(__DIR__.'/libs/crest/settings.json')) {
	CRestPlus::installApp();
	$bindData = array('event'   => 'onCrmActivityAdd','handler' => 'https://demo.nicedo.ru/local/php_interface/rodion/handlers/event_handler.php');
	CRestPlus::call('event.bind', $bindData);
}

if (isset($_GET['send'])) {
	$dateToTmp   = $_GET['date-to'] ?: date('Y-m-d');
	$dateFrom = $_GET['date-from'] ?: date('Y-m-d');
	$dateTo = date('Y-m-d', (strtotime($dateToTmp) + 86400));

	$users = CRestPlus::callBatchList('user.get', array());
	foreach ($users as $user) {
		foreach ($user['result']['result'] as $value) {
			foreach ($value as $v)
				$getUsers[$v['ID']]['name'] = $v['LAST_NAME'].' '.$v['NAME'];
		}
	}

	$sql = 'SELECT * FROM lead_table WHERE date_create>="'.$dateFrom.'" AND date_create<="'.$dateTo.'"';
	if (!$result = $mysqli->query($sql)) die($mysqli->error);
	while( $row = mysqli_fetch_assoc($result))
        $data[] = $row;
    $result->free();

    foreach ($data as $value) {
    	$main[$value['responsible']]['name'] = $getUsers[$value['responsible']]['name'];
	    $main[$value['responsible']]['for_view'] += 1;
	    if(strtotime($value['date_update']) > 0)
	    	$main[$value['responsible']]['update'] += 1;
    }

    $sql = 'SELECT * FROM block_table WHERE date_block BETWEEN "'.$dateFrom.'" AND "'.$dateTo.'"';
	if (!$result = $mysqli->query($sql)) die($mysqli->error);
	while ($row = mysqli_fetch_assoc($result))
		$dataBlk[] = $row;
	$result->free();
	foreach ($dataBlk as $value) {
		$main[$value['responsible']]['block'] += 1;
		$main[$value['responsible']]['status'] = !($value['date_unblock'] > 0) ? 'block' : 'unblock';
	}

	### удаляем старые записи
	$sql = 'DELETE FROM lead_table WHERE date_create<="'.date('Y-m-d', strtotime('-180days')).'"';
	if (!$result = $mysqli->query($sql)) die($mysqli->error);
}
$mysqli->close();
require_once __DIR__.'/table/index.php';