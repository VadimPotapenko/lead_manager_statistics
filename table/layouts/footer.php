			
			<div class='clearfix'></div>
		</div>
		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
		<script src="table/bootstrap/js/bootstrap.js" type="text/javascript"></script>
		<script src="https://api.bitrix24.com/api/v1/"></script>
        <script src='table/js/sort.js'></script>
		<script>
			BX24.init(function () {
                BX24.resizeWindow(
                    $( document ).width(), 
                    $( document ).height() + 100 > 800 ? $( "#application" ).height() + 100 : 800,
                    function (data) {}
                );
            });

            function sendBack(int) {
                let bttn = this.children[this.children.length - 1].children[0];
                bttn.style.display = 'none';
                let idManager = this.getAttribute('data-id')
            	this.classList.remove('block');
                this.classList.add('unblock');
                $.ajax({
            		type: 'POST',
            		url: 'handlers/event_free_user.php',
            		data: {id:idManager},
            		success: function(data){
            		  console.log(data);
        		    }
                });
            }

            function run () {
                let row = document.querySelector('tbody');
                for (let i = 0; i < row.children.length; i++)
                    row.children[i].onclick = sendBack;
            }
            run();
        </script>
	</body>
</html>