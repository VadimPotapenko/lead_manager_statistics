		<?php include(__DIR__.'/layouts/header.php'); ?>
			<header class='container-fluid'>
				<p class='text-black h4 text-center'>Отчет по блокировке</p>
				<form class='text-black' method='get'>
					<div class='row'>
						<div class="form-group col">
							<label for="formGroupExampleInput">Дата с:</label>
							<input type="date" name='date-from' class="form-control" id="formGroupExampleInput" placeholder="Пример пункта формы" value="<?=$_GET['date-from'];?>">
						</div>
						<div class="form-group col">
							<label for="formGroupExampleInput2">Дата до:</label>
							<input type="date" name='date-to' class="form-control" id="formGroupExampleInput2" placeholder="Пример пункта формы" value="<?=$_GET['date-to'];?>">
						</div>
					</div>

					<div class='row'>
						<div class='col-1'>
							<input type="submit" name="send" class='btn btn-success m-2'>
						</div>
					</div>
				</form>
			</header>
			<main class='container-fluid bg-light'>
				<table class='table text-center table-hover table_sort'>
					<thead>
						<tr>
							<th>Сотрудник</th>
							<th>Открыто на просмотр</th>
							<th>Изменено уникальных лидов</th>
							<th>Заблокирован</th>
							<th>Действие</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($main as $key => $value): ?>
							<tr class="<?=$value['status'];?>" data-id="<?=$key;?>">
								<td><?=$value['name'] ?: '0';?></td>
								<td><?=$value['for_view'] ?: '0';?></td>
								<td><?=$value['update'] ?: '0';?></td>
								<td><?=$value['block'] ?: '0';?></td>
								<?php if($value['status'] == 'block'): ?>
									<td><button class='btn bg-danger text-white'>Разблокировать</button></td>
								<?php else: ?>
									<td></td>
								<?php endif; ?>
							</tr>
						<?php endforeach; ?>
					</tbody>
					<tfoot class='text-white'>
					</tfoot>
				</table>

				
			</main>
		<?php include(__DIR__.'/layouts/footer.php'); ?>