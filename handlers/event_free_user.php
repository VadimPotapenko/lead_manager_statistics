<?php
require_once dirname(__DIR__).'/db_conf.php';
if (isset($_POST['id'])) {
	$mysqli = new mysqli(DB_CONF['host'], DB_CONF['user'], DB_CONF['pass'], DB_CONF['db']);
	if ($mysqli->connect_errno) die($mysqli->connect_error);
	$sql = 'UPDATE lead_table SET is_worked=1, date_update="'.date('Y-m-d H:i:s').'" WHERE responsible='.$_POST['id'];
	if (!$result = $mysqli->query($sql)) die($mysqli->error);

	$sql = 'UPDATE block_table SET date_unblock="'.date('Y-m-d H:i:s').'"';
	if (!$result = $mysqli->query($sql)) die($mysqli->error);
	$mysqli->close();
	echo $_POST['id'];
}
