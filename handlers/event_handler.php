<?php
require_once dirname(__DIR__).'/libs/crest/CRestPlus.php';
require_once dirname(__DIR__).'/libs/debugger/Debugger.php';
require_once dirname(__DIR__).'/db_conf.php';
$mysqli = new mysqli(DB_CONF['host'], DB_CONF['user'], DB_CONF['pass'], DB_CONF['db']);
if ($mysqli->connect_errno) die($mysqli->connect_error);
if (isset($_REQUEST['event']) && $_REQUEST['event'] == 'ONCRMACTIVITYADD') {
	$activity = CRestPlus::call('crm.activity.get', array('ID' => $_REQUEST['data']['FIELDS']['ID']));
	### Проверяем что это исходящий звонок
	if ($activity['result']['DIRECTION'] == '2' && $activity['result']['OWNER_TYPE_ID'] == '1') {
		### Каждую найденную запись помечаем отработанной = Y
		$sql = 'UPDATE lead_table SET is_worked=1, date_update="'.date('Y-m-d H:i:s').'" WHERE responsible='.$activity['result']['RESPONSIBLE_ID'].' AND is_worked=0 AND lead_id='.$activity['result']['OWNER_ID'];
		if (!$result = $mysqli->query($sql)) die($mysqli->error);
	}
}
$mysqli->close();