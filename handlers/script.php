<?php
AddEventHandler('main', 'OnProlog', 'blockUserFunction');
function blockUserFunction () {
	global $USER;
	global $APPLICATION;
	require_once dirname(__DIR__).'/db_conf.php';
	require_once dirname(__DIR__).'/libs/crest/CRestPlus.php';
	require_once dirname(__DIR__).'/libs/debugger/Debugger.php';
	### настройки уведомления
	define ('TO', '40');             // кому уведомления
	define ('MESSAGE' , $USER->GetFirstName().' '.$USER->GetLastName().' WTF?');     // текст уведомления
	define ('COUNT_BLOCK', '3');     // количество разрешенных просмотров
	define ('ADMINS', array('1'));   // id пользователей на которых скрипт не работает
	$mysqli = new mysqli(DB_CONF['host'], DB_CONF['user'], DB_CONF['pass'], DB_CONF['db']);
	if ($mysqli->connect_errno) die($mysqli->connect_error);

	### получаем id ответственного и лида
	if (preg_match('~/[0-9-]+/~', $_SERVER['REQUEST_URI'])) {
		$currentUser = $USER->getID();
		$leadId = explode('/', $_SERVER['REQUEST_URI'])[4];
		if (!in_array($currentUser, ADMINS)) {
			### Проверять количество НЕ отработанных лидов, если найдено более 10 записей
			$sql = 'SELECT max(id) FROM lead_table WHERE is_worked=1';
			if (!$result = $mysqli->query($sql)) die($mysqli->error);
			while ($row = mysqli_fetch_assoc($result))
				$data[] = $row;

			if (empty($data[0]['max(id)'])) $continueSql = ';';
			else $continueSql = 'AND id>'.$data[0]['max(id)'].';';
			$sql = 'SELECT * FROM lead_table WHERE responsible='.$currentUser.' AND is_worked=0 '.$continueSql;
			if (!$result = $mysqli->query($sql)) die($mysqli->error);
			while( $row = mysqli_fetch_assoc($result))
		        $data[] = $row;
		    foreach ($data as $value)
		    	$workLead[] = $value['lead_id'];


		    $sql = 'SELECT * FROM block_table WHERE responsible='.$currentUser;
		    if (!$blockResult = $mysqli->query($sql)) die($mysqli->error);
			while( $row = mysqli_fetch_assoc($blockResult))
		        $blockData[] = $row;
			if (($result->num_rows >= COUNT_BLOCK) && !in_array($leadId, $workLead) && ($blockData[0]['date_unblock'] != 0 || !isset($blockData))) {
				### Совершить блокировку и Отправить уведомления определенным пользователям
				$sql = 'INSERT INTO block_table (responsible, date_block) VALUES ("'.$currentUser.'", "'.date('Y-m-d H:i:s').'")';
				if (!$resultSecond = $mysqli->query($sql)) die($mysqli->error);
				$sendMessage = CRestPlus::call('im.notify', array('to' => TO, 'message' => MESSAGE, 'type' => 'SYSTEM'));
			}
			$result->free();

			### проверка блокирован ли пользователь
			$sql = 'SELECT * FROM block_table WHERE responsible='.$currentUser.' AND date_unblock=0';
			if (!$result = $mysqli->query($sql)) die($mysqli->error);
			if (($result->num_rows > 0) && !in_array($leadId, $workLead)) {
				$APPLICATION->AddHeadString(
	                "<script data-tag='custom-company-read-only'>
						let container = document.getElementById('workarea-content') ? document.getElementById('workarea-content') : document.querySelector('.crm-iframe-content');
						container.style.display = 'none';
	                </script>"
	            );
				$result->free();
				
			} else {
				### SQL запись в lead-table если такой нет
				$sql = 'SELECT * FROM lead_table WHERE lead_id='.$leadId.' AND responsible='.$currentUser;
				if (!$result = $mysqli->query($sql)) die($mysqli->error);
				if ($result->num_rows === 0) {
					$sql = 'INSERT INTO lead_table (lead_id, is_worked, responsible, date_create) VALUES ('.$leadId.', 0, '.$currentUser.', "'.date('Y-m-d H:i:s').'")';
					if (!$result = $mysqli->query($sql)) die($mysqli->error);
				}
			}
		}
	}
	$mysqli->close();
}